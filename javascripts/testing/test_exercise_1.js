/*
 * Dev: arevalo.a.05@gmail.com
 */


QUnit.test( "Add new user", function( assert ) {

  document.getElementById("add_user").value = 'Arevalo Araque';
  document.getElementById("btn_add").click();
  assert.ok(document.getElementById("table_users").rows.length == 5,"The user was added successfully");

});

QUnit.test( "Click in add user without value", function( assert ) {

  document.getElementById("btn_add").click();
  assert.ok(document.getElementById("table_users").rows.length == 4,"No user for added");

});

QUnit.test( "Click in delete user", function( assert ) {

  document.getElementById("1").click();
  assert.ok(document.getElementById("table_users").rows.length == 3,"User witd id=1 deleted");

});