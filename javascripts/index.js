/*
 * Dev: arevalo.a.05@gmail.com
 */

// Get all elements from base.html for set in template
var getElementsFromBase = document.querySelector('link[rel=import]');

// Set menu tags
var getMenu = getElementsFromBase.import.querySelector('#menu_list');
document.getElementById('menu').appendChild(document.importNode(getMenu, true));

var table_users = document.getElementById('table_users');
countUsers = 0;
var usersIds = [];

// Count total users
countTotalUsers = function (){
	total_user = table_users.rows.length;
	var totaluser = document.getElementById('totaluser').innerHTML = total_user;
}

function calculateNextId (ids) {
	var maxValue = 0;

	for (i=0; i<ids.length ; i++){
	    if (ids[i] > maxValue) {
	        var maxValue = ids[i];
	    }
	}

	return maxValue+1;
}

document.addEventListener('click',function(e){
	var element = event.target;
    if(element.tagName == 'BUTTON' && element.classList.contains("btn_del")){
        var id = element.id;
        var row = document.getElementById('tr'+id);
    	row.parentNode.removeChild(row);
    	countTotalUsers();
    }
});

countTotalUsers();
countUsers = table_users.rows.length;