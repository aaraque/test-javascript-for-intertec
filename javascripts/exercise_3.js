/*
 * Dev: arevalo.a.05@gmail.com
 */

// Select elements
var body = document.getElementsByTagName('body')[0];
var btn_add = document.getElementById('btn_add');
var btn_del = document.getElementsByClassName('btn_del');
var btn_reset = document.getElementById('btn_reset');
var add_user = document.getElementById('add_user');
var content = document.getElementsByClassName('content_users');
var content_user_detail = document.getElementsByClassName('content_user_detail');

var users = {};
var posts = {};

// create database local using indexedDB
var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
var db = null;

function startLocalDB () {
	db = indexedDB.open("usersList",1);
	db.onupgradeneeded = function (e) {
		var active = db.result;
		var object = active.createObjectStore("userData",{ keyPath : "id"});
		object.createIndex("name");
		object.createIndex("username");
		object.createIndex("posts");
	};

	db.onsuccess = function (e) {
	    console.log("Database loaded succesfully...");
	    loadUsersList();
	};

	db.onerror = function (e)  {
	    console.log("Error while load the database...");
	};
}

function addToLocalStorage(id,name,username,posts) {
    var active = db.result;
    var data = active.transaction(["userData"], "readwrite");
    var object = data.objectStore("userData");

    var request = object.put({
    	"id" : id,
    	"name":name,
    	"username":username,
    	"posts":posts
    });

    request.onerror = function (e) {
        console.log(request.error.name + " - " + request.error.message);
    };

    data.oncomplete = function (e) {
        console.log("Object saved succesfully");
    };
}

function loadUsersList () {
	var active = db.result;
	var data = active.transaction(["userData"], "readonly");
	var object = data.objectStore("userData");

	var users_list = [];

	object.openCursor().onsuccess = function (e) {

	    var result = e.target.result;
	    if (result === null) {
	        return;
	    }

	    users_list.push(result.value);
	    result.continue();
	};

	data.oncomplete = function () {
		users = users_list;
		if (users_list.length == 0) {
			getJsonUsersAndPosts();
		} else {
	        for (var index in users_list) {
	        	usersIds.push(users_list[index].id);
        		var newRow = table_users.insertRow(0);
        		newRow.id = 'tr'+users_list[index].id;
        		var td = newRow.insertCell(0);
        		countTotalUsers();
        		td.innerHTML = '<input type="text" readonly="true" class="textfield" value="'+users_list[index].name+' - ('+users_list[index].posts.length+' Posts)">&nbsp;<button class="btn btn_del" id="'+users_list[index].id+'">Delete</button><button class="btn btn_detail" id="details_'+users_list[index].id+'">Details</button>';
	        }
	        document.getElementById('load_info').hidden = true;
	        countUsers = countTotalUsers();
	    }
    };
}

function deleteUser (id) {
	var active = db.result;
	var data = active.transaction(["userData"], "readwrite");
	var object = data.objectStore("userData");

	var result = object.delete(parseInt(id));

	result.onsuccess = function () {
		console.log("Object deleted succesfully");
	};
}

function deleteAllObject () {
	var active = db.result;
	var data = active.transaction(["userData"], "readwrite");

	var object = data.objectStore("userData");

  	var objectStoreRequest = object.clear();

  	objectStoreRequest.onsuccess = function(event) {
    	console.log("Data cleared succesfully");
  	};
}


startLocalDB();

// END - create database local using indexedDB

function getJsonUsersAndPosts() {
	var xmlhttp = new XMLHttpRequest();
	var url = "http://jsonplaceholder.typicode.com/users";

	// show "load" info
	document.getElementById('load_info').hidden = false;
	document.getElementById('status_load').hidden = true;

	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	        users = JSON.parse(xmlhttp.responseText);
	        var xmlhttp_posts = new XMLHttpRequest();
			
			var url_posts = "http://jsonplaceholder.typicode.com/posts";

			xmlhttp_posts.onreadystatechange = function() {
			    if (xmlhttp_posts.readyState == 4 && xmlhttp_posts.status == 200) {
			        posts = JSON.parse(xmlhttp_posts.responseText);

			        for (var i=0;i<users.length;i++) {
			        	users[i]['posts'] = [];
			        	for (var x=0;x<posts.length;x++) {
			        		if (users[i].id == posts[x].userId) {
			        			users[i]['posts'].push(posts[x]);
			        		}

			        		if (x == posts.length-1){
					        	posts_for_ls = (users[i]["posts"].length > 0)?users[i]["posts"]:[];
					        	addToLocalStorage(users[i].id,users[i].name,users[i].username,posts_for_ls);
			        		}
			        	};

			        	if (i == (users.length-1)) {
			        		// Build table with users
			        		for (var z=0;z<users.length;z++) {
			        			usersIds.push(users[z].id);
				        		var newRow = table_users.insertRow(0);
				        		newRow.id = 'tr'+users[z].id;
				        		var td = newRow.insertCell(0);
				        		td.innerHTML = '<input type="text" readonly="true" class="textfield" value="'+users[z].name+' - ('+users[z].posts.length+' Posts)">&nbsp;<button class="btn btn_del" id="'+users[z].id+'">Delete</button><button class="btn btn_detail" id="details_'+users[z].id+'">Details</button>';
				        		countTotalUsers();
			        		};

			        		document.getElementById('load_info').hidden = true;
			        	}
			        };
			        countUsers = countTotalUsers();
			    } else if (xmlhttp_posts.readyState == 4 && xmlhttp_posts.status != 200) {
			    	document.getElementById('status_load').querySelector('i').innerHTML = 'An error found while trying load the posts';
			    	document.getElementById('status_load').hidden = false;
			    	document.getElementById('load_info').hidden = true;
			    }
			};
			xmlhttp_posts.open("GET", url_posts, true);
			xmlhttp_posts.send();
	    } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
	    	document.getElementById('status_load').querySelector('i').innerHTML = 'An error found while trying load the users';
	    	document.getElementById('status_load').hidden = false;
	    	document.getElementById('load_info').hidden = true;
	    }
	};
	xmlhttp.open("GET", url, true);
	xmlhttp.send();

}

btn_reset.addEventListener('click',function(){
	table_users.innerHTML = '';
	countTotalUsers();
	deleteAllObject();
	getJsonUsersAndPosts();
});

document.addEventListener('click',function(e){
	var element = event.target;
    if(element.tagName == 'BUTTON' && element.classList.contains("btn_detail")){
        var id = element.id.substring(8,element.id.length);
        for (var i=0;i<users.length;i++) {
        	if (users[i].id == id) {
        		content[0].style.display = 'none';
				content_user_detail[0].style.display = 'block';

				document.getElementById('name').innerHTML = ''+users[i].name + ' ('+users[i].username+')';
	        	if (users[i]['posts'].length > 0) {
		        	for (var x=0;x<users[i]['posts'].length;x++) {
		        		var li = document.createElement("li");
		        		li.appendChild(document.createTextNode(''+users[i]['posts'][x].id+'. '+users[i]['posts'][x].title));
		        		document.getElementById('list_posts').appendChild(li);
		        	};
		        }
		        break;
        	}
        };
    }

    if (element.tagName == 'BUTTON' && element.classList.contains("btn_return")){
    	content[0].style.display = 'block';
		content_user_detail[0].style.display = 'none';
		document.getElementById('name').innerHTML = '';
		document.getElementById('list_posts').innerHTML = '';
    }
});

btn_add.addEventListener('click',function(){
	var val_add_user = add_user.value;
	if (!val_add_user) {
		return false;
	} else {
		countUsers = calculateNextId(usersIds);
		usersIds.push(countUsers);
		var newRow = table_users.insertRow(0);
		newRow.id = 'tr'+countUsers;
		var td = newRow.insertCell(0);
		td.innerHTML = '<input type="text" readonly="true" class="textfield" value="'+val_add_user+' - (0 Posts)">&nbsp;<button class="btn btn_del" id="'+countUsers+'">Delete</button><button class="btn btn_detail" id="details_'+countUsers+'">Details</button>';
		add_user.value = '';
		countTotalUsers();
		var user = {"id": countUsers,"name": val_add_user,"posts":[],"username":val_add_user};
		users.push(user);
		addToLocalStorage(countUsers,val_add_user,val_add_user,[]);
	}
});

document.addEventListener('click',function(e){
	var element = event.target;
    if(element.tagName == 'BUTTON' && element.classList.contains("btn_del")){
        var id = element.id;
        deleteUser(id);
    }
});