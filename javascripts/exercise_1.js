/*
 * Dev: arevalo.a.05@gmail.com
 */

// Get Onclick in Add user
var btn_add = document.getElementById('btn_add');
var add_user = document.getElementById('add_user');

btn_add.addEventListener('click',function(){
	val_add_user = add_user.value;
	
	if (val_add_user) {
		countUsers++;
		var newRow = table_users.insertRow(0);
		newRow.id = 'tr'+countUsers;
		var td = newRow.insertCell(0);
		td.innerHTML = '<input type="text" readonly="true" class="textfield" value="'+val_add_user+' - (0 Posts)">&nbsp;<button class="btn btn_del" id="'+countUsers+'">Delete</button>';
		add_user.value = '';
	}
	
	countTotalUsers();

	if (val_add_user == '') {
		return false;
	} 

});