/*
 * Dev: arevalo.a.05@gmail.com
 */

// Get Onclick in Add user
var btn_add = document.getElementById('btn_add');
var btn_del = document.getElementsByClassName('btn_del');
var btn_reset = document.getElementById('btn_reset');
var add_user = document.getElementById('add_user');

var users = {};
var posts = {};

function getJsonUsersAndPosts() {
	var xmlhttp = new XMLHttpRequest();
	var url = "http://jsonplaceholder.typicode.com/users";

	// show "load" info
	document.getElementById('load_info').hidden = false;
	document.getElementById('status_load').hidden = true;

	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	        users = JSON.parse(xmlhttp.responseText);
	        var xmlhttp_posts = new XMLHttpRequest();
			
			var url_posts = "http://jsonplaceholder.typicode.com/posts";

			xmlhttp_posts.onreadystatechange = function() {
			    if (xmlhttp_posts.readyState == 4 && xmlhttp_posts.status == 200) {
			        posts = JSON.parse(xmlhttp_posts.responseText);

			        for (var i=0;i<users.length;i++) {
			        	users[i]['posts'] = [];
			        	for (var x=0;x<posts.length;x++) {
			        		if (users[i].id == posts[x].userId) {
			        			users[i]['posts'].push(posts[x]);
			        		}
			        	};

			        	if (i == (users.length-1)) {
			        		// Build table with users
			        		for (var z=0;z<users.length;z++) {
			        			usersIds.push(users[z].id);
				        		var newRow = table_users.insertRow(0);
				        		newRow.id = 'tr'+users[z].id;
				        		var td = newRow.insertCell(0);
				        		td.innerHTML = '<input type="text" readonly="true" class="textfield" value="'+users[z].name+' - ('+users[z].posts.length+' Posts)">&nbsp;<button class="btn btn_del" id="'+users[z].id+'">Delete</button>';
				        		countTotalUsers();
			        		};

			        		document.getElementById('load_info').hidden = true;
			        	}
			        };
			        countUsers = calculateNextId(usersIds);
			    } else if (xmlhttp_posts.readyState == 4 && xmlhttp_posts.status != 200) {
			    	document.getElementById('status_load').querySelector('i').innerHTML = 'An error found while trying load the posts';
			    	document.getElementById('status_load').hidden = false;
			    	document.getElementById('load_info').hidden = true;
			    }
			};
			xmlhttp_posts.open("GET", url_posts, true);
			xmlhttp_posts.send();
	    } else if (xmlhttp.readyState == 4 && xmlhttp.status != 200) {
	    	document.getElementById('status_load').querySelector('i').innerHTML = 'An error found while trying load the users';
	    	document.getElementById('status_load').hidden = false;
	    	document.getElementById('load_info').hidden = true;
	    }
	};
	xmlhttp.open("GET", url, true);
	xmlhttp.send();

}

getJsonUsersAndPosts();

btn_reset.addEventListener('click',function(){
	table_users.innerHTML = '';
	countTotalUsers();
	getJsonUsersAndPosts();
});